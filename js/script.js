document.addEventListener('DOMContentLoaded', () => {

    const themeBtn = document.getElementById('theme');
    const logo = document.getElementById('logo');
    const theme = localStorage.getItem('theme');

    if (theme == 'light') {
        document.body.classList.add('light-theme');
        switchImg('moon', 'dark');
    } else {
        document.body.classList.remove('light-theme');
        switchImg('sun', 'light');
    }

    themeBtn.addEventListener('click', () => {
        document.body.classList.toggle('light-theme');
    
        if (document.body.classList.contains('light-theme')) {
            localStorage.setItem('theme', 'light');
            switchImg('moon', 'dark');
        } else {
            localStorage.setItem('theme', 'dark');
            switchImg('sun', 'light');
        }
    });

    function switchImg(themeImg, logoImg) {
        themeBtn.src = `icons/${themeImg}.png`;
        logo.src = `img/sk-infrastructure-logo-${logoImg}.png`;
    }
});


